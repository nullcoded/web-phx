defmodule Nullcoded.Repo do
  use Ecto.Repo,
    otp_app: :nullcoded,
    adapter: Ecto.Adapters.Postgres
end
